package com.tsc.smironova.tm.command.union;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;

public class ProjectClearCommand extends AbstractProjectTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        getProjectTaskService().clearProjects();
    }

}
