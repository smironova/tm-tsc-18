package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.IUserRepository;
import com.tsc.smironova.tm.api.service.IUserService;
import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.exception.empty.*;
import com.tsc.smironova.tm.exception.user.UserExistsWithEmailException;
import com.tsc.smironova.tm.exception.user.UserExistsWithLoginException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.HashUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(final String login, final String password) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (ValidationUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (isLoginExists(login))
            throw new UserExistsWithLoginException(login);
        final User user = new User(login, HashUtil.salt(password), Role.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (ValidationUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (ValidationUtil.isEmpty(email))
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserExistsWithEmailException(email);
        final User user = create(login, password);
        if (user == null)
            return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (ValidationUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (ValidationUtil.isEmpty(role))
            throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null)
            return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (ValidationUtil.isEmpty(userId))
            throw new EmptyIdException();
        if (ValidationUtil.isEmpty(password))
            throw new EmptyPasswordException();
        final User user = findOneById(userId);
        if (user == null)
            return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User setRole(final String userId, final Role role) {
        if (ValidationUtil.isEmpty(userId))
            throw new EmptyIdException();
        if (ValidationUtil.isEmpty(role))
            throw new EmptyRoleException();
        final User user = findOneById(userId);
        if (user == null)
            return null;
        user.setRole(role);
        return user;
    }

    @Override
    public void remove(final User user) {
        if (ValidationUtil.isEmpty(user))
            return;
        userRepository.remove(user);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User findOneById(final String userId) {
        if (ValidationUtil.isEmpty(userId))
            throw new EmptyIdException();
        return userRepository.findOneById(userId);
    }

    @Override
    public User findOneByLogin(final String login) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.findOneByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) {
        if (ValidationUtil.isEmpty(email))
            throw new EmptyEmailException();
        return userRepository.findOneByEmail(email);
    }

    @Override
    public User removeOneById(final String userId) {
        if (ValidationUtil.isEmpty(userId))
            throw new EmptyIdException();
        return userRepository.removeOneById(userId);
    }

    @Override
    public User removeOneByLogin(final String login) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.removeOneByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        return !ValidationUtil.isEmpty(findOneByLogin(login));
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (ValidationUtil.isEmpty(email))
            throw new EmptyLoginException();
        return !ValidationUtil.isEmpty(findOneByEmail(email));
    }

    @Override
    public User updateOneById(final String userId, final String lastName, final String firstName, final String middleName, final String email) {
        if (ValidationUtil.isEmpty(userId))
            throw new EmptyIdException();
        if (ValidationUtil.isEmpty(lastName))
            throw new EmptyLastNameException();
        if (ValidationUtil.isEmpty(firstName))
            throw new EmptyFirstNameException();
        if (ValidationUtil.isEmpty(middleName))
            throw new EmptyMiddleNameException();
        if (ValidationUtil.isEmpty(email))
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserExistsWithEmailException(email);
        final User user = findOneById(userId);
        if (user == null)
            return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateOneByLogin(final String login, final String lastName, final String firstName, final String middleName, final String email) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (ValidationUtil.isEmpty(lastName))
            throw new EmptyLastNameException();
        if (ValidationUtil.isEmpty(firstName))
            throw new EmptyFirstNameException();
        if (ValidationUtil.isEmpty(middleName))
            throw new EmptyMiddleNameException();
        if (ValidationUtil.isEmpty(email))
            throw new EmptyEmailException();
        if (isEmailExists(email))
            throw new UserExistsWithEmailException(email);
        final User user = findOneByLogin(login);
        if (user == null)
            return null;
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

}
