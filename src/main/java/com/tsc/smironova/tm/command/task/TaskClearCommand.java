package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_CLEAR;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        getTaskService().clear();
    }

}
