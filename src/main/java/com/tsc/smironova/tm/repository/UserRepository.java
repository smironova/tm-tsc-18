package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.repository.IUserRepository;
import com.tsc.smironova.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public void add(final User user) {
        users.add(user);
    }

    @Override
    public void remove(final User user) {
        users.remove(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User findOneById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId()))
                return user;
        }
        return null;
    }

    @Override
    public User findOneByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User findOneByEmail(final String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail()))
                return user;
        }
        return null;
    }

    @Override
    public User removeOneById(final String id) {
        final User user = findOneById(id);
        if (user == null)
            return null;
        users.remove(user);
        return user;
    }

    @Override
    public User removeOneByLogin(final String login) {
        final User user = findOneByLogin(login);
        if (user == null)
            return null;
        users.remove(user);
        return user;
    }

}
