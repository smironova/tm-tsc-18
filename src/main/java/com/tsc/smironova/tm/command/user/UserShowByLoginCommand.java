package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserShowByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_VIEW_BY_LOGIN;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_VIEW_BY_LOGIN;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth())
            throw new AccessDeniedException();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = getUserService().findOneByLogin(login);
        if (user == null)
            throw new UserNotFoundException();
        if (!getAuthService().getCurrentUserId().equals(user.getId()) || getAuthService().isAdmin())
            throw new AccessDeniedException();
        showUser(user);
    }

}
