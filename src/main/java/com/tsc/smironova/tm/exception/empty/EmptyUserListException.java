package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyUserListException extends AbstractException {

    public EmptyUserListException() {
        super("No users! Add new user...");
    }

}
