package com.tsc.smironova.tm.exception.user;

import com.tsc.smironova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
