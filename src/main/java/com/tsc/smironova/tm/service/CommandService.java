package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.ICommandRepository;
import com.tsc.smironova.tm.api.service.ICommandService;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.exception.empty.EmptyNameException;

import java.util.Collection;

import static com.tsc.smironova.tm.util.ValidationUtil.isEmpty;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (isEmpty(name))
            throw new EmptyNameException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (isEmpty(arg))
            return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandName();
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null)
            return;
        commandRepository.add(command);
    }

}
