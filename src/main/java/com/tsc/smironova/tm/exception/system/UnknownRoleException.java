package com.tsc.smironova.tm.exception.system;

import com.tsc.smironova.tm.exception.AbstractException;

public class UnknownRoleException extends AbstractException {

    public UnknownRoleException(final String role) {
        super("Error! Sort ``" + role + "`` was not found...");
    }

}
