package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyProjectIdException extends AbstractException {

    public EmptyProjectIdException() {
        super("Error! Project ID is empty...");
    }

}
