package com.tsc.smironova.tm.api.repository;

import com.tsc.smironova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    void add(User user);

    void remove(User user);

    void clear();

    User findOneById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User removeOneById(String id);

    User removeOneByLogin(String login);

}
