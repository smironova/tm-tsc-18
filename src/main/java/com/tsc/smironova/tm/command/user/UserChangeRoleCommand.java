package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.system.UnknownSortException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_CHANGE_ROLE;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_CHANGE_ROLE;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth())
            throw new AccessDeniedException();
        System.out.println("[CHANGE USER ROLE]");
        System.out.println("ENTER ID:");
        final String userId = TerminalUtil.nextLine();
        final User user = getUserService().findOneById(userId);
        if (user == null)
            throw new UserNotFoundException();
        if (!getAuthService().getCurrentUserId().equals(user.getId()) || getAuthService().isAdmin())
            throw new AccessDeniedException();
        System.out.println("ENTER ROLE:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Role.values()) + ColorUtil.RESET);
        final String roleId = TerminalUtil.nextLine();
        if (ValidationUtil.checkRole(roleId.toUpperCase()))
            throw new UnknownSortException(roleId);
        final Role role = Role.valueOf(roleId.toUpperCase());
        final User userChange = getUserService().setRole(userId, role);
        if (userChange == null)
            throw new UserNotFoundException();
        showUser(userChange);
    }

}
