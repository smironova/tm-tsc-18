package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
