package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_CREATE;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_CREATE;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = getUserService().create(login, password, email);
        if (user == null)
            throw new UserNotFoundException();
        showUser(user);
    }

}
