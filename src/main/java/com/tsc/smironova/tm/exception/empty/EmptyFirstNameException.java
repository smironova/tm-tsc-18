package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error! First name is empty...");
    }

}
