package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserUpdateProfileByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_UPDATE_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth())
            throw new AccessDeniedException();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = getUserService().findOneById(id);
        if (user == null)
            throw new UserNotFoundException();
        if (!getAuthService().getCurrentUserId().equals(user.getId()))
            throw new AccessDeniedException();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User userChange = getUserService().updateOneById(id, lastName, firstName, middleName, email);
        if (userChange == null)
            throw new UserNotFoundException();
        showUser(userChange);
    }

}
