package com.tsc.smironova.tm.command.auth;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;

public class LogoutCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.LOGOUT;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.LOGOUT;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getAuthService().logout();
    }

}
