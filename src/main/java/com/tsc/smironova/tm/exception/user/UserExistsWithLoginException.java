package com.tsc.smironova.tm.exception.user;

import com.tsc.smironova.tm.exception.AbstractException;

public class UserExistsWithLoginException extends AbstractException {

    public UserExistsWithLoginException(final String login) {
        super("Error! Such user ``" + login + "`` already exists...");
    }

}
