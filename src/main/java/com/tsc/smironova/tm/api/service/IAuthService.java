package com.tsc.smironova.tm.api.service;

import com.tsc.smironova.tm.model.User;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

    boolean isAuth();

    boolean isAdmin();

    User login(String login, String password);

    void logout();

}
