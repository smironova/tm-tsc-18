package com.tsc.smironova.tm.exception.system;

import com.tsc.smironova.tm.exception.AbstractException;

public class UnknownStatusException extends AbstractException {

    public UnknownStatusException(final String status) {
        super("Error! Sort ``" + status + "`` was not found...");
    }

}
