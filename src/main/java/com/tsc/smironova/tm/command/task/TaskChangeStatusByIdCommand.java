package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.exception.entity.TaskNotFoundException;
import com.tsc.smironova.tm.exception.system.UnknownSortException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_UPDATE_STATUS_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_UPDATE_STATUS_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        if (ValidationUtil.checkSort(statusId.toUpperCase()))
            throw new UnknownSortException(statusId);
        final Status status = Status.valueOf(statusId.toUpperCase());
        final Task task = getTaskService().changeTaskStatusById(id, status);
        if (task == null)
            throw new TaskNotFoundException();
    }

}
