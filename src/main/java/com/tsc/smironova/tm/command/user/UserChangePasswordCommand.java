package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_CHANGE_PASSWORD;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_CHANGE_PASSWORD;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth())
            throw new AccessDeniedException();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER ID:");
        final String userId = TerminalUtil.nextLine();
        final User user = getUserService().findOneById(userId);
        if (user == null)
            throw new UserNotFoundException();
        if (!getAuthService().getCurrentUserId().equals(user.getId()))
            throw new AccessDeniedException();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userChange = getUserService().setPassword(userId, password);
        if (userChange == null)
            throw new UserNotFoundException();
        showUser(userChange);
    }

}
