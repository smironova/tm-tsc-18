package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_REMOVE_BY_LOGIN;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_REMOVE_BY_LOGIN;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth() || getAuthService().isAdmin())
            throw new AccessDeniedException();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        if (getUserService().findOneByLogin(login).getId().equals(getAuthService().getCurrentUserId()))
            throw new AccessDeniedException();
        final User user = getUserService().removeOneByLogin(login);
        if (user == null)
            throw new UserNotFoundException();
        showUser(user);
    }

}
