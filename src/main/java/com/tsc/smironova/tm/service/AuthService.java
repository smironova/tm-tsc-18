package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.IAuthRepository;
import com.tsc.smironova.tm.api.service.IAuthService;
import com.tsc.smironova.tm.api.service.IUserService;
import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.exception.empty.EmptyLoginException;
import com.tsc.smironova.tm.exception.empty.EmptyPasswordException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.HashUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

public class AuthService implements IAuthService {

    private final IAuthRepository authRepository;
    private final IUserService userService;

    public AuthService(final IAuthRepository authRepository, final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        final String userId = authRepository.getCurrentUserId();
        if (ValidationUtil.isEmpty(userId))
            return null;
        return userId;
    }

    @Override
    public void setCurrentUserId(final String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        return ValidationUtil.isEmpty(authRepository.getCurrentUserId());
    }

    @Override
    public boolean isAdmin() {
        return !(userService.findOneById(getCurrentUserId()).getRole().equals(Role.ADMIN));
    }

    @Override
    public User login(final String login, final String password) {
        if (ValidationUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (ValidationUtil.isEmpty(password))
            throw new EmptyPasswordException();
        final User user = userService.findOneByLogin(login);
        if (user == null)
            return null;
        final String hash = HashUtil.salt(password);
        if (hash == null)
            throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash()))
            throw new AccessDeniedException();
        setCurrentUserId(user.getId());
        return user;
    }

    @Override
    public void logout() {
        if (isAuth())
            throw new AccessDeniedException();
        setCurrentUserId(null);
    }

}
