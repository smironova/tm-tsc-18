package com.tsc.smironova.tm.command.system;

import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.constant.ArgumentConstant;
import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.util.SystemOutUtil;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConstant.ARG_ABOUT;
    }

    @Override
    public String name() {
        return TerminalConstant.ABOUT;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.ABOUT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
        SystemOutUtil.printLine();
    }

}
