package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.TaskNotFoundException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_START_BY_NAME;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_START_BY_NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = getTaskService().startTaskByName(name);
        if (task == null)
            throw new TaskNotFoundException();
    }

}
