package com.tsc.smironova.tm.command.auth;

import com.tsc.smironova.tm.api.service.IAuthService;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.ValidationUtil;

public abstract class AbstractAuthCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user) {
        if (user == null)
            throw new UserNotFoundException();
        System.out.println("[USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        if (!ValidationUtil.isEmpty(user.getLastName()))
            System.out.println("NAME: " + user.getLastName() + " " + user.getFirstName() + " " + user.getMiddleName());
        if (!ValidationUtil.isEmpty(user.getEmail()))
            System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
