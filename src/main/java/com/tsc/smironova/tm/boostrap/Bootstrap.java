package com.tsc.smironova.tm.boostrap;

import com.tsc.smironova.tm.api.repository.*;
import com.tsc.smironova.tm.api.service.*;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.command.auth.LoginCommand;
import com.tsc.smironova.tm.command.auth.LogoutCommand;
import com.tsc.smironova.tm.command.project.*;
import com.tsc.smironova.tm.command.system.*;
import com.tsc.smironova.tm.command.task.*;
import com.tsc.smironova.tm.command.union.*;
import com.tsc.smironova.tm.command.union.ProjectClearCommand;
import com.tsc.smironova.tm.command.task.TaskUpdateByIdCommand;
import com.tsc.smironova.tm.command.user.*;
import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.exception.empty.EmptyCommandException;
import com.tsc.smironova.tm.exception.system.UnknownArgumentException;
import com.tsc.smironova.tm.exception.system.UnknownCommandException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.repository.*;
import com.tsc.smironova.tm.service.*;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthRepository authRepository = new AuthRepository();
    private final IAuthService authService = new AuthService(authRepository, userService);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ArgumentsShowCommand());
        registry(new ExitCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectRemoveWithTasksByIdCommand());
        registry(new ProjectRemoveWithTasksByIndexCommand());
        registry(new ProjectRemoveWithTasksByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());

        registry(new TaskListShowCommand());
        registry(new TasksShowAllByProjectIdCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());

        registry(new UserListShowCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserShowByIdCommand());
        registry(new UserShowByLoginCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateProfileByIdCommand());
        registry(new UserUpdateProfileByLoginCommand());
    }

    public void initUsers() {
        userService.create("test", "test", "test@email.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("user_test", "user_test");
    }

    public void run(final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        loggerService.debug("!!! TEST !!!");
        initUsers();
        if (parseArgs(args))
            new ExitCommand().execute();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                SystemOutUtil.printOkMessage();
            }
            catch (Exception e) {
                loggerService.error(e);
                SystemOutUtil.printFailMessage();
            }
        }
    }

    public void registry(final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseCommand(final String cmd) {
        if (ValidationUtil.isEmpty(cmd))
            throw new EmptyCommandException();
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null)
            throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void parseArg(final String arg) {
        if (ValidationUtil.isEmpty(arg))
            return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null)
            throw new UnknownArgumentException(arg);
        command.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (ValidationUtil.isEmpty(args))
            return false;
        final String arg = args[0];
        parseArg(arg);
        return  true;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
