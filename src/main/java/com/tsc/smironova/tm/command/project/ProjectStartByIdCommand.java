package com.tsc.smironova.tm.command.project;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_START_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_START_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().startProjectById(id);
        if (project == null)
            throw new ProjectNotFoundException();
    }

}
