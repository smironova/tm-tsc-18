package com.tsc.smironova.tm.api.service;

import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User setRole(String userId, Role role);

    void remove(User user);

    void clear();

    User findOneById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User removeOneById(String id);

    User removeOneByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User updateOneById(String userId, String lastName, String firstName, String middleName, String email);

    User updateOneByLogin(String login, String lastName, String firstName, String middleName, String email);

}
