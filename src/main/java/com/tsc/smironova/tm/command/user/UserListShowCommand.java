package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.empty.EmptyUserListException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.List;

public class UserListShowCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_LIST;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_LIST;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth() || getAuthService().isAdmin())
            throw new AccessDeniedException();
        System.out.println("[USER LIST]");
        final List<User> users = getUserService().findAll();
        if (ValidationUtil.isEmpty(users))
            throw new EmptyUserListException();
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

}
