package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyTaskListException extends AbstractException {

    public EmptyTaskListException() {
        super("No tasks! Add new task...");
    }

}
