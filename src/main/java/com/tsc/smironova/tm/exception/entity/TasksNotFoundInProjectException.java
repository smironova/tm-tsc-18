package com.tsc.smironova.tm.exception.entity;

import com.tsc.smironova.tm.exception.AbstractException;

public class TasksNotFoundInProjectException extends AbstractException {

    public TasksNotFoundInProjectException(final String projectId) {
        super("Error! No tasks in project " + projectId + "...");
    }

}
