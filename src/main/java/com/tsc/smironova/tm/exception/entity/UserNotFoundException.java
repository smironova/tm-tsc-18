package com.tsc.smironova.tm.exception.entity;

import com.tsc.smironova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User was not found...");
    }

}
