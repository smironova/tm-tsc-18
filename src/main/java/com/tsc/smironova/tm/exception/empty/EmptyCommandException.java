package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
