package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.TaskNotFoundException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_REMOVE_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_REMOVE_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().removeOneById(id);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

}
